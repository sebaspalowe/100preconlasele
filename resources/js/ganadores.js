var semana1=[
  {
    "NO": 1,
    "NOMBRE": "Víctor Ureña",
    "CEDULA": "8-345-611",
    "PRODUCTO": "+Móvil prepago"
  },
  {
    "NO": 2,
    "NOMBRE": "Walther Cárdenas",
    "CEDULA": "E-8-103085",
    "PRODUCTO": "+Móvil postpago"
  },
  {
    "NO": 3,
    "NOMBRE": "Jessica Miranda",
    "CEDULA": "4-765-2363",
    "PRODUCTO": "Residencial"
  },
  {
    "NO": 4,
    "NOMBRE": "Reynaldo Bairnals",
    "CEDULA": "8-270-437",
    "PRODUCTO": "+Móvil prepago"
  },
  {
    "NO": 5,
    "NOMBRE": "Alonso Ceballo",
    "CEDULA": "8-723-986",
    "PRODUCTO": "+Móvil postpago"
  },
  {
    "NO": 6,
    "NOMBRE": "María Eugenia Amaya",
    "CEDULA": "N-19-148",
    "PRODUCTO": "+Móvil postpago"
  },
  {
    "NO": 7,
    "NOMBRE": "Edwin De Gracia",
    "CEDULA": "4-721-1381",
    "PRODUCTO": "+Móvil postpago"
  },
  {
    "NO": 8,
    "NOMBRE": "Rafael Antonio Navas",
    "CEDULA": "8-325-309",
    "PRODUCTO": "+TV Digital prepago"
  },
  {
    "NO": 9,
    "NOMBRE": "Roberto Gutiérrez",
    "CEDULA": "4-112-906",
    "PRODUCTO": "+Móvil postpago"
  },
  {
    "NO": 10,
    "NOMBRE": "Ernesto Cedeño",
    "CEDULA": "8-818-2366",
    "PRODUCTO": "+Móvil prepago"
  },
  {
    "NO": 11,
    "NOMBRE": "Olonigtalipe Garcia",
    "CEDULA": "10-711-153",
    "PRODUCTO": "+Móvil prepago"
  },
  {
    "NO": 12,
    "NOMBRE": "Juan De Dios Pimentel",
    "CEDULA": "9-721-207",
    "PRODUCTO": "+Móvil prepago"
  },
  {
    "NO": 13,
    "NOMBRE": "Felix Guevara",
    "CEDULA": "8-232-453",
    "PRODUCTO": "+Móvil postpago"
  },
  {
    "NO": 14,
    "NOMBRE": "Juan José Zhang Chon",
    "CEDULA": "8-884-109",
    "PRODUCTO": "+Móvil prepago"
  },
  {
    "NO": 15,
    "NOMBRE": "Eugenia Vivero",
    "CEDULA": "8-725-1655",
    "PRODUCTO": "+Móvil prepago"
  },
  {
    "NO": 16,
    "NOMBRE": "Kiran Kumar Ahir",
    "CEDULA": "E-8-90642",
    "PRODUCTO": "Residencial"
  },
  {
    "NO": 17,
    "NOMBRE": "Neil Villar",
    "CEDULA": "8-867-1009",
    "PRODUCTO": "+Móvil prepago"
  },
  {
    "NO": 18,
    "NOMBRE": "Iluminada Puga",
    "CEDULA": "9-702-1568",
    "PRODUCTO": "+Móvil prepago"
  },
  {
    "NO": 19,
    "NOMBRE": "Yulissa Carrera",
    "CEDULA": "7-711-2050",
    "PRODUCTO": "+Móvil prepago"
  },
  {
    "NO": 20,
    "NOMBRE": "Paulina Avila",
    "CEDULA": "9-216-671",
    "PRODUCTO": "+Móvil prepago"
  },
  {
    "NO": 21,
    "NOMBRE": "Víctor Chang",
    "CEDULA": "8-239-955",
    "PRODUCTO": "+Móvil prepago"
  },
  {
    "NO": 22,
    "NOMBRE": "Rosa Abrego",
    "CEDULA": "9-57-929",
    "PRODUCTO": "Residencial"
  },
  {
    "NO": 23,
    "NOMBRE": "Orquis Fernández",
    "CEDULA": "P-37-5380",
    "PRODUCTO": "Residencial"
  },
  {
    "NO": 24,
    "NOMBRE": "Janette Pinzón",
    "CEDULA": "2-110-841",
    "PRODUCTO": "+Móvil prepago"
  },
  {
    "NO": 25,
    "NOMBRE": "Tomasa Altagracia",
    "CEDULA": "PE-711-499",
    "PRODUCTO": "+Móvil prepago"
  },
  {
    "NO": 26,
    "NOMBRE": "Pedro Garcia",
    "CEDULA": "8-201-728",
    "PRODUCTO": "+Móvil prepago"
  },
  {
    "NO": 27,
    "NOMBRE": "Wilfredo  Flores",
    "CEDULA": "8-761-2148",
    "PRODUCTO": "+Móvil prepago"
  },
  {
    "NO": 28,
    "NOMBRE": "Benigno Hernández",
    "CEDULA": "AT-329237",
    "PRODUCTO": "+Móvil prepago"
  },
  {
    "NO": 29,
    "NOMBRE": "Marlon Muñoz",
    "CEDULA": "8-819-1638",
    "PRODUCTO": "MASTER"
  },
  {
    "NO": 30,
    "NOMBRE": "Tairyn Troetsh",
    "CEDULA": "4-738-579",
    "PRODUCTO": "Residencial"
  },
  {
    "NO": 31,
    "NOMBRE": "Yolanda Aizpú",
    "CEDULA": "8-188-884",
    "PRODUCTO": "+Móvil prepago"
  },
  {
    "NO": 32,
    "NOMBRE": "Griselda de Herrera",
    "CEDULA": "8-164-2000",
    "PRODUCTO": "+Móvil postpago"
  },
  {
    "NO": 33,
    "NOMBRE": "Maria Cristina Cruz López",
    "CEDULA": "8-237-2135",
    "PRODUCTO": "+Móvil postpago"
  },
  {
    "NO": 34,
    "NOMBRE": "Anabel Barsallo",
    "CEDULA": "9-705-239",
    "PRODUCTO": "+Móvil prepago"
  },
  {
    "NO": 35,
    "NOMBRE": "Manuel Colorado",
    "CEDULA": "FB-350737",
    "PRODUCTO": "+Móvil prepago"
  },
  {
    "NO": 36,
    "NOMBRE": "Mary Arjona",
    "CEDULA": "8-931-795",
    "PRODUCTO": "+Móvil postpago"
  },
  {
    "NO": 37,
    "NOMBRE": "Jorge Gómez Martínez",
    "CEDULA": "3-35-347",
    "PRODUCTO": "+Móvil prepago"
  },
  {
    "NO": 38,
    "NOMBRE": "Luis Alfredo Vásquez",
    "CEDULA": "8-982-229",
    "PRODUCTO": "+Móvil prepago"
  },
  {
    "NO": 39,
    "NOMBRE": "Luis  Ramos",
    "CEDULA": "8-769-2138",
    "PRODUCTO": "+Móvil prepago"
  },
  {
    "NO": 40,
    "NOMBRE": "Kirvan Jesús López",
    "CEDULA": "4-775-2212",
    "PRODUCTO": "+Móvil prepago"
  },
  {
    "NO": 41,
    "NOMBRE": "Daniel Oblitas",
    "CEDULA": "E-8-120807",
    "PRODUCTO": "+Móvil postpago"
  },
  {
    "NO": 42,
    "NOMBRE": "Carlos Valencia Garcia",
    "CEDULA": "AU-433965",
    "PRODUCTO": "+Móvil postpago"
  },
  {
    "NO": 43,
    "NOMBRE": "Bolivia Povea",
    "CEDULA": "8-200-2179",
    "PRODUCTO": "Residencial"
  },
  {
    "NO": 44,
    "NOMBRE": "Rosaura Troetsh",
    "CEDULA": "4-150-13",
    "PRODUCTO": "+Móvil prepago"
  },
  {
    "NO": 45,
    "NOMBRE": "Víctor Mc Elfresshs",
    "CEDULA": "8-755-203",
    "PRODUCTO": "+Móvil prepago"
  },
  {
    "NO": 46,
    "NOMBRE": "María Aguila",
    "CEDULA": "8-768-1956",
    "PRODUCTO": "+Móvil postpago"
  },
  {
    "NO": 47,
    "NOMBRE": "Lady Delgado",
    "CEDULA": "N-201800",
    "PRODUCTO": "+Movil Postpago"
  },
  {
    "NO": 48,
    "NOMBRE": "Hector Ringaldo",
    "CEDULA": "SG354285",
    "PRODUCTO": "Residencial"
  },
  {
    "NO": 49,
    "NOMBRE": "Modesta Ruda",
    "CEDULA": "8-186-126",
    "PRODUCTO": "Master"
  },
  {
    "NO": 50,
    "NOMBRE": "Digna Moreno",
    "CEDULA": "8-529-2151",
    "PRODUCTO": "+Movil Prepago"
  },
  {
    "NO": 51,
    "NOMBRE": "Euclides Aguilar",
    "CEDULA": "8-130-395",
    "PRODUCTO": "Residencial"
  },
  {
    "NO": 52,
    "NOMBRE": "Jesús Boras",
    "CEDULA": "8-765-1065",
    "PRODUCTO": "Master"
  },
  {
    "NO": 53,
    "NOMBRE": "Máximo López",
    "CEDULA": "9-158-566",
    "PRODUCTO": "+TV Digital Prepago"
  },
  {
    "NO": 54,
    "NOMBRE": "Jia Hui Lou",
    "CEDULA": "E-8-156991",
    "PRODUCTO": "+Movil Prepago"
  },
  {
    "NO": 55,
    "NOMBRE": "Kathia Rivera",
    "CEDULA": "4-753-2203",
    "PRODUCTO": "+Movil Prepago"
  },
  {
    "NO": 56,
    "NOMBRE": "Bélgica Rodríguez",
    "CEDULA": "3-702-998",
    "PRODUCTO": "+Movil Prepago"
  },
  {
    "NO": 57,
    "NOMBRE": "Daysi de Modes",
    "CEDULA": "9-126-124",
    "PRODUCTO": "Residencial"
  },
  {
    "NO": 58,
    "NOMBRE": "Luis Vega",
    "CEDULA": "8-778-150",
    "PRODUCTO": "+Movil Postpago"
  },
  {
    "NO": 59,
    "NOMBRE": "Lémesis Castillo",
    "CEDULA": "6-714-1558",
    "PRODUCTO": "+Movil Prepago"
  },
  {
    "NO": 60,
    "NOMBRE": "Héctor Berbei",
    "CEDULA": "7-38-98",
    "PRODUCTO": "+Movil Prepago"
  },
  {
    "NO": 61,
    "NOMBRE": "Fernando Berroa",
    "CEDULA": "PE-12-57",
    "PRODUCTO": "+Movil Prepago"
  },
  {
    "NO": 62,
    "NOMBRE": "Judith Palacios",
    "CEDULA": "8-737-95",
    "PRODUCTO": "Master"
  },
  {
    "NO": 63,
    "NOMBRE": "José Smith",
    "CEDULA": "8-702-923",
    "PRODUCTO": "+Movil Prepago"
  },
  {
    "NO": 64,
    "NOMBRE": "Evelina de Boyd",
    "CEDULA": "9-55-833",
    "PRODUCTO": "+Movil Postpago"
  },
  {
    "NO": 65,
    "NOMBRE": "Frederick Espinoza",
    "CEDULA": "4-813-597",
    "PRODUCTO": "+Movil Prepago"
  },
  {
    "NO": 66,
    "NOMBRE": "Zuleika De León",
    "CEDULA": "9-728-774",
    "PRODUCTO": "+Movil Prepago"
  },
  {
    "NO": 67,
    "NOMBRE": "Alicia Afú",
    "CEDULA": "8-24-5924",
    "PRODUCTO": "Master"
  },
  {
    "NO": 68,
    "NOMBRE": "Edgardo Cartagena",
    "CEDULA": "3-111-135",
    "PRODUCTO": "+Movil Prepago"
  },
  {
    "NO": 69,
    "NOMBRE": "Carlos Miniel",
    "CEDULA": "8-817-1383",
    "PRODUCTO": "+Movil Prepago"
  },
  {
    "NO": 70,
    "NOMBRE": "Nadine Loo",
    "CEDULA": "8-819-1354",
    "PRODUCTO": "+Movil Prepago"
  },
  {
    "NO": 71,
    "NOMBRE": "Marlon Perlaza",
    "CEDULA": "8-844-233",
    "PRODUCTO": "+Movil Prepago"
  },
  {
    "NO": 72,
    "NOMBRE": "Juan  Gomez",
    "CEDULA": "8-777-1154",
    "PRODUCTO": "+Movil Prepago"
  },
  {
    "NO": 73,
    "NOMBRE": "José Hun",
    "CEDULA": "8-96-483",
    "PRODUCTO": "Master"
  },
  {
    "NO": 74,
    "NOMBRE": "Roberto Fernández",
    "CEDULA": "8-953-2347",
    "PRODUCTO": "+Movil Prepago"
  },
  {
    "NO": 75,
    "NOMBRE": "Elsa Mercado",
    "CEDULA": "SB311811",
    "PRODUCTO": "+Movil Prepago"
  },
  {
    "NO": 76,
    "NOMBRE": "Juana Vega",
    "CEDULA": "7-92-1034",
    "PRODUCTO": "+Movil Prepago"
  },
  {
    "NO": 77,
    "NOMBRE": "Marlon Ríos",
    "CEDULA": "8-229-2060",
    "PRODUCTO": "+Movil Prepago"
  },
  {
    "NO": 78,
    "NOMBRE": "Yamileth Fuentes",
    "CEDULA": "8-725-256",
    "PRODUCTO": "+Movil Prepago"
  },
  {
    "NO": 79,
    "NOMBRE": "Xenia Choy",
    "CEDULA": "4-243-126",
    "PRODUCTO": "+Movil Postpago"
  },
  {
    "NO": 80,
    "NOMBRE": "Milciades Infante",
    "CEDULA": "10-7-2147",
    "PRODUCTO": "+Movil Postpago"
  },
  {
    "NO": 81,
    "NOMBRE": "José Thompson",
    "CEDULA": "3-708-296",
    "PRODUCTO": "+Movil Prepago"
  },
  {
    "NO": 82,
    "NOMBRE": "Abraham Gutierrez",
    "CEDULA": "10-705-125",
    "PRODUCTO": "Residencial"
  },
  {
    "NO": 83,
    "NOMBRE": "Carol Baxber",
    "CEDULA": "4-725-316",
    "PRODUCTO": "+Movil Prepago"
  },
  {
    "NO": 84,
    "NOMBRE": "Luciano Guerra",
    "CEDULA": "9-134-794",
    "PRODUCTO": "Residencial"
  },
  {
    "NO": 85,
    "NOMBRE": "Yulieth Meneses",
    "CEDULA": "3-737-297",
    "PRODUCTO": "+Movil Prepago"
  },
  {
    "NO": 86,
    "NOMBRE": "Abdiel Ortega",
    "CEDULA": "3-734-2132",
    "PRODUCTO": "+Movil Prepago"
  },
  {
    "NO": 87,
    "NOMBRE": "Stephanie Castro",
    "CEDULA": "8-909-418",
    "PRODUCTO": "Residencial"
  },
  {
    "NO": 88,
    "NOMBRE": "Justo Saavedra",
    "CEDULA": "6-21-297",
    "PRODUCTO": "+Movil Prepago"
  },
  {
    "NO": 89,
    "NOMBRE": "Olmedo Gil",
    "CEDULA": "8-330-735",
    "PRODUCTO": "+Movil Prepago"
  },
  {
    "NO": 90,
    "NOMBRE": "Luis Miguel Cárdenas",
    "CEDULA": "8-786-236",
    "PRODUCTO": "+Movil Prepago"
  },
  {
    "NO": 91,
    "NOMBRE": "Roberto Gordón",
    "CEDULA": "3-79-2275",
    "PRODUCTO": "+Movil Prepago"
  },
  {
    "NO": 92,
    "NOMBRE": "Evaristo Mojica",
    "CEDULA": "9-141-447",
    "PRODUCTO": "+Movil Prepago"
  },
  {
    "NO": 93,
    "NOMBRE": "Benjamín Melo",
    "CEDULA": "8-858-2087",
    "PRODUCTO": "+Movil Prepago"
  },
  {
    "NO": 94,
    "NOMBRE": "Ernesto León",
    "CEDULA": "8-161-45",
    "PRODUCTO": "Master"
  },
  {
    "NO": 95,
    "NOMBRE": "Yerlin Andreve",
    "CEDULA": "8-849-1349",
    "PRODUCTO": "+Movil Prepago"
  },
  {
    "NO": 96,
    "NOMBRE": "Nidia de Villanueva",
    "CEDULA": "8-286-22",
    "PRODUCTO": "+Movil Prepago"
  },
  {
    "NO": 97,
    "NOMBRE": "Osvaldo Quintero",
    "CEDULA": "12-702-931",
    "PRODUCTO": "+Movil Prepago"
  },
  {
    "NO": 98,
    "NOMBRE": "Andres Araúz",
    "CEDULA": "4-198-490",
    "PRODUCTO": "+Movil Prepago"
  },
  {
    "NO": 99,
    "NOMBRE": "Aralis Fernández",
    "CEDULA": "8-815-2024",
    "PRODUCTO": "+Movil Prepago"
  },
  {
    "NO": 100,
    "NOMBRE": "Angel Moreno",
    "CEDULA": "6-58-155",
    "PRODUCTO": "+Movil Prepago"
  }
];
$(document).ready( function () {
        $('#tablaganadores').DataTable(
          {
            data:semana1,
            select: false,
            columns: [
                  { data: 'NO' },
                  { data: 'NOMBRE' },
                  { data: 'CEDULA' }
                 // { data: 'PRODUCTO' }
              ],
            ordering: false,
            info:     false,
            searching:false,
            pageLength: 25,
            responsive: true,
            pagingType: "simple_numbers",
            lengthChange: false
          });
        today = new Date()
        dayIndex = today.getDay();

        var fechas=[["15","3","includes/img/iphone.png","15 de marzo","100"],
            ["22","3","includes/img/televisores.png","22 de marzo","100"],
            ["28","3","includes/img/tarjetas.png","28 de marzo","100"],
            ["5","4","includes/img/camiseta_oficial.png","5 de abril","100"],
            ["12","4","includes/img/camiseta_oficial.png","12 de abril","100"],
            ["19","4","includes/img/camiseta_oficial.png","19 de abril","100"],
            ["26","4","includes/img/iphone.png","26 de abril","100"],
            ["3","5","includes/img/camiseta_oficial.png","3 de mayo","100"],
            ["10","5","includes/img/amistoso.png","10 de mayo","100"],
            ["17","5","includes/img/camiseta_oficial.png","17 de mayo","100"],
            ["24","5","includes/img/camiseta_oficial.png","24 de mayo","100"],
            ["31","5","includes/img/iphone.png","31 de mayo","100"],
            ["7","6","includes/img/tarjetas.png","7 de junio","100"],
            ["14","6","includes/img/iphone.png","14 de junio","100"],
            ["21","6","includes/img/camiseta_oficial.png","21 de junio","100"],
            ["28","6","includes/img/camiseta_oficial.png","28 de junio","100"],
            ["5","7","includes/img/camiseta_oficial.png","5 de julio","100"],
            ["12","7","includes/img/camiseta_oficial.png","12 de julio","100"]
          ]
        var meses=["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
        Date.prototype.getWeek = function() {
          var onejan = new Date(this.getFullYear(),0,1);
          return Math.ceil((((this - onejan) / 86400000) + onejan.getDay()+1)/7);
        }

        var today = new Date(),month = today.getMonth()+1, day= today.getDate(),premio, oneWeekAgo,fecha;
         // Returns the week number as an integer
        jQuery.each(fechas,function(index,value){
            fecha = new Date(2018,value[1]-1,value[0]);
            oneWeekAgo = new Date(2018,value[1]-1,value[0]);
            oneWeekAgo.setDate(oneWeekAgo.getDate() - 7);
            today.setDate(today.getDate() - 7);
            if(oneWeekAgo < today && today <= fecha)
            {
               premio='<div class="col-xs-5 col-sm-7 cant"><div class="col-sm-4 num"><p class="big red-f">'+value[4]+'</p></div><div class="col-sm-8  borde"><img class="center-block" src="'+value[2]+'" /></div></div><div class="col-xs-7 col-sm-5"><p class="h2">Tómbola <br> <span class="red-f">'+value[3]+'</span></p></div>';
            }
           
        });
        console.log(premio);
        jQuery("#fechapasada").html(premio);
    } );