jQuery(document).ready(function ($) {

	if (pagina == 'home') {

		//Scroll down
		function scrollMenu(targetElement) {
			$('html, body').animate({
				scrollTop: $('#' + targetElement).offset().top - 60
			}, 1000);
		}
		$(".btn-menu").on("click", function () {
			var targetElement = $(this).data('targetmenu');
			scrollMenu(targetElement);
			return false;
		});

		//Cuenta regresiva
		var clock = '';
		var clock = $('.clock').FlipClock(diferencia_tiempo_mundial, {
			clockFace: 'DailyCounter',
			countdown: true
		});

		//Premios Semanales del Home
		var fechas = [
			["15", "3", base_url + "public/img/premio4.png", "15 de marzo", "100"],
			["22", "3", base_url + "public/img/premio5.png", "22 de marzo", "100"],
			["28", "3", base_url + "public/img/premio1.png", "29 de marzo", "100"],
			["5", "4", base_url + "public/img/premio3.png", "5 de abril", "100"],
			["12", "4", base_url + "public/img/premio3.png", "12 de abril", "100"],
			["19", "4", base_url + "public/img/premio3.png", "19 de abril", "100"],
			["26", "4", base_url + "public/img/premio4.png", "26 de abril", "100"],
			["3", "5", base_url + "public/img/premio3.png", "3 de mayo", "100"],
			["10", "5", base_url + "public/img/premio2.png", "10 de mayo", "100"],
			["17", "5", base_url + "public/img/premio3.png", "17 de mayo", "100"],
			["24", "5", base_url + "public/img/premio3.png", "24 de mayo", "100"],
			["31", "5", base_url + "public/img/premio3.png", "31 de mayo", "100"],
			["7", "6", base_url + "public/img/premio4.png", "7 de junio", "100"],
			["14", "6", base_url + "public/img/premio1.png", "14 de junio", "100"],
			["21", "6", base_url + "public/img/premio3.png", "21 de junio", "100"],
			["28", "6", base_url + "public/img/premio3.png", "28 de junio", "100"],
			["5", "7", base_url + "public/img/premio3.png", "5 de julio", "100"],
			["12", "7", base_url + "public/img/premio3.png", "12 de julio", "100"]
		]
		var meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
		Date.prototype.getWeek = function () {
			var onejan = new Date(this.getFullYear(), 0, 1);
			return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
		}

		var today = new Date(),
			month = today.getMonth() + 1,
			day = today.getDate(),
			premio, oneWeekAgo, fecha;
		// Returns the week number as an integer

		jQuery.each(fechas, function (index, value) {
			fecha = new Date(2018, value[1] - 1, value[0], 23, 59, 59);

			oneWeekAgo = new Date(2018, value[1] - 1, value[0], 23, 59, 59);
			oneWeekAgo.setDate(oneWeekAgo.getDate() - 6);
			console.log("Fecha----------")


			console.log(oneWeekAgo);
			console.log(today);
			console.log(fecha);
			if (oneWeekAgo < today && today <= fecha) {
				console.log("gano------------")
				premio = '<div class="col-xs-7 cant"><div class="col-sm-4 num"><p class="big">' + value[4] + '</p></div><div class="col-sm-8  borde"><img class="center-block" src="' + value[2] + '" /></div></div><div class="col-xs-5"><p class="h2">Tómbola <br> <span>' + value[3] + '</span></p></div>';


			}

		});
		//console.log(premio);
		jQuery("#fechafutura").html(premio);


	} else if (pagina == 'ganadores') {

		var semana1 = [{
				"NO": 1,
				"NOMBRE": "Víctor Ureña",
				"CEDULA": "8-345-611",
				"PRODUCTO": "Móvil prepago"
			},
			{
				"NO": 2,
				"NOMBRE": "Walther Cárdenas",
				"CEDULA": "E-8-103085",
				"PRODUCTO": "Móvil postpago"
			},
			{
				"NO": 3,
				"NOMBRE": "Jessica Miranda",
				"CEDULA": "4-765-2363",
				"PRODUCTO": "Residencial"
			},
			{
				"NO": 4,
				"NOMBRE": "Reynaldo Bairnals",
				"CEDULA": "8-270-437",
				"PRODUCTO": "Móvil prepago"
			},
			{
				"NO": 5,
				"NOMBRE": "Alonso Ceballo",
				"CEDULA": "8-723-986",
				"PRODUCTO": "Móvil postpago"
			},
			{
				"NO": 6,
				"NOMBRE": "María Eugenia Amaya",
				"CEDULA": "N-19-148",
				"PRODUCTO": "Móvil postpago"
			},
			{
				"NO": 7,
				"NOMBRE": "Edwin De Gracia",
				"CEDULA": "4-721-1381",
				"PRODUCTO": "Móvil postpago"
			},
			{
				"NO": 8,
				"NOMBRE": "Rafael Antonio Navas",
				"CEDULA": "8-325-309",
				"PRODUCTO": "+TV Digital prepago"
			},
			{
				"NO": 9,
				"NOMBRE": "Roberto Gutierrez",
				"CEDULA": "4-112-906",
				"PRODUCTO": "Móvil postpago"
			},
			{
				"NO": 10,
				"NOMBRE": "Ernesto Cedeño",
				"CEDULA": "8-818-2366",
				"PRODUCTO": "Móvil prepago"
			},
			{
				"NO": 11,
				"NOMBRE": "Olonigtalipe Garcia",
				"CEDULA": "10-711-153",
				"PRODUCTO": "Móvil prepago"
			},
			{
				"NO": 12,
				"NOMBRE": "Juan De Dios Pimentel",
				"CEDULA": "9-721-207",
				"PRODUCTO": "Móvil prepago"
			},
			{
				"NO": 13,
				"NOMBRE": "Felix Guevara",
				"CEDULA": "8-232-453",
				"PRODUCTO": "Móvil postpago"
			},
			{
				"NO": 14,
				"NOMBRE": "Juan José Zhang Chon",
				"CEDULA": "8-884-109",
				"PRODUCTO": "Móvil prepago"
			},
			{
				"NO": 15,
				"NOMBRE": "Eugenia Vivero",
				"CEDULA": "8-725-1655",
				"PRODUCTO": "Móvil prepago"
			},
			{
				"NO": 16,
				"NOMBRE": "Kiran Kumar Ahir",
				"CEDULA": "E-8-90642",
				"PRODUCTO": "Residencial"
			},
			{
				"NO": 17,
				"NOMBRE": "Neil Villar",
				"CEDULA": "8-867-1009",
				"PRODUCTO": "Móvil prepago"
			},
			{
				"NO": 18,
				"NOMBRE": "Iluminada Puga",
				"CEDULA": "9-702-1568",
				"PRODUCTO": "Móvil prepago"
			},
			{
				"NO": 19,
				"NOMBRE": "Yulissa Carrera",
				"CEDULA": "7-711-2050",
				"PRODUCTO": "Móvil prepago"
			},
			{
				"NO": 20,
				"NOMBRE": "Paulina Avila",
				"CEDULA": "9-216-671",
				"PRODUCTO": "Móvil prepago"
			},
			{
				"NO": 21,
				"NOMBRE": "Víctor Chang",
				"CEDULA": "8-239-955",
				"PRODUCTO": "Móvil prepago"
			},
			{
				"NO": 22,
				"NOMBRE": "Rosa Abrego",
				"CEDULA": "9-57-929",
				"PRODUCTO": "Residencial"
			},
			{
				"NO": 23,
				"NOMBRE": "Orquis Fernández",
				"CEDULA": "P-37-5380",
				"PRODUCTO": "Residencial"
			},
			{
				"NO": 24,
				"NOMBRE": "Janette Pinzón",
				"CEDULA": "2-110-841",
				"PRODUCTO": "Móvil prepago"
			},
			{
				"NO": 25,
				"NOMBRE": "Tomasa Altagracia",
				"CEDULA": "PE-711-499",
				"PRODUCTO": "Móvil prepago"
			},
			{
				"NO": 26,
				"NOMBRE": "Pedro Garcia",
				"CEDULA": "8-201-728",
				"PRODUCTO": "Móvil prepago"
			},
			{
				"NO": 27,
				"NOMBRE": "Wilfredo  Flores",
				"CEDULA": "8-761-2148",
				"PRODUCTO": "Móvil prepago"
			},
			{
				"NO": 28,
				"NOMBRE": "Benigno Hernández",
				"CEDULA": "AT-329237",
				"PRODUCTO": "Móvil prepago"
			},
			{
				"NO": 29,
				"NOMBRE": "Marlon Muñoz",
				"CEDULA": "8-819-1638",
				"PRODUCTO": "MASTER"
			},
			{
				"NO": 30,
				"NOMBRE": "Tairyn Troetsh",
				"CEDULA": "4-738-579",
				"PRODUCTO": "Residencial"
			},
			{
				"NO": 31,
				"NOMBRE": "Yolanda Aizpú",
				"CEDULA": "8-188-884",
				"PRODUCTO": "Móvil prepago"
			},
			{
				"NO": 32,
				"NOMBRE": "Griselda de Herrera",
				"CEDULA": "8-164-2000",
				"PRODUCTO": "Móvil postpago"
			},
			{
				"NO": 33,
				"NOMBRE": "Maria Cristina Cruz López",
				"CEDULA": "8-237-2135",
				"PRODUCTO": "Móvil postpago"
			},
			{
				"NO": 34,
				"NOMBRE": "Anabel Barsallo",
				"CEDULA": "9-705-239",
				"PRODUCTO": "Móvil prepago"
			},
			{
				"NO": 35,
				"NOMBRE": "Manuel Colorado",
				"CEDULA": "FB-350737",
				"PRODUCTO": "Móvil prepago"
			},
			{
				"NO": 36,
				"NOMBRE": "Mary Arjona",
				"CEDULA": "8-931-795",
				"PRODUCTO": "Móvil postpago"
			},
			{
				"NO": 37,
				"NOMBRE": "Jorge Gómez Martínez",
				"CEDULA": "3-35-347",
				"PRODUCTO": "Móvil prepago"
			},
			{
				"NO": 38,
				"NOMBRE": "Luis Alfredo Vásquez",
				"CEDULA": "8-982-229",
				"PRODUCTO": "Móvil prepago"
			},
			{
				"NO": 39,
				"NOMBRE": "Luis Ramos",
				"CEDULA": "8-769-2138",
				"PRODUCTO": "Móvil prepago"
			},
			{
				"NO": 40,
				"NOMBRE": "Kirvan Jesús López",
				"CEDULA": "4-775-2212",
				"PRODUCTO": "Móvil prepago"
			},
			{
				"NO": 41,
				"NOMBRE": "Daniel Oblitas",
				"CEDULA": "E-8-120807",
				"PRODUCTO": "Móvil postpago"
			},
			{
				"NO": 42,
				"NOMBRE": "Carlos Valencia Garcia",
				"CEDULA": "AU-433965",
				"PRODUCTO": "Móvil postpago"
			},
			{
				"NO": 43,
				"NOMBRE": "Bolivia Povea",
				"CEDULA": "8-200-2179",
				"PRODUCTO": "Residencial"
			},
			{
				"NO": 44,
				"NOMBRE": "Rosaura Troetsh",
				"CEDULA": "4-150-13",
				"PRODUCTO": "Móvil prepago"
			},
			{
				"NO": 45,
				"NOMBRE": "Víctor Mc Elfresshs",
				"CEDULA": "8-755-203",
				"PRODUCTO": "Móvil prepago"
			},
			{
				"NO": 46,
				"NOMBRE": "María Aguila",
				"CEDULA": "8-768-1956",
				"PRODUCTO": "Móvil postpago"
			},
			{
				"NO": 47,
				"NOMBRE": "Lady Delgado",
				"CEDULA": "N-201800",
				"PRODUCTO": "+Movil Postpago"
			},
			{
				"NO": 48,
				"NOMBRE": "Hector Ringaldo",
				"CEDULA": "SG354285",
				"PRODUCTO": "Residencial"
			},
			{
				"NO": 49,
				"NOMBRE": "Modesta Ruda",
				"CEDULA": "8-186-126",
				"PRODUCTO": "Master"
			},
			{
				"NO": 50,
				"NOMBRE": "Digna Moreno",
				"CEDULA": "8-529-2151",
				"PRODUCTO": "+Movil Prepago"
			},
			{
				"NO": 51,
				"NOMBRE": "Euclides Aguilar",
				"CEDULA": "8-130-395",
				"PRODUCTO": "Residencial"
			},
			{
				"NO": 52,
				"NOMBRE": "Jesús Boras",
				"CEDULA": "8-765-1065",
				"PRODUCTO": "Master"
			},
			{
				"NO": 53,
				"NOMBRE": "Máximo López",
				"CEDULA": "9-158-566",
				"PRODUCTO": "+TV Digital Prepago"
			},
			{
				"NO": 54,
				"NOMBRE": "Jia Hui Lou",
				"CEDULA": "E-8-156991",
				"PRODUCTO": "+Movil Prepago"
			},
			{
				"NO": 55,
				"NOMBRE": "Kathia Rivera",
				"CEDULA": "4-753-2203",
				"PRODUCTO": "+Movil Prepago"
			},
			{
				"NO": 56,
				"NOMBRE": "Bélgica Rodríguez",
				"CEDULA": "3-702-998",
				"PRODUCTO": "+Movil Prepago"
			},
			{
				"NO": 57,
				"NOMBRE": "Daysi de Modes",
				"CEDULA": "9-126-124",
				"PRODUCTO": "Residencial"
			},
			{
				"NO": 58,
				"NOMBRE": "Luis Vega",
				"CEDULA": "8-778-150",
				"PRODUCTO": "+Movil Postpago"
			},
			{
				"NO": 59,
				"NOMBRE": "Lémesis Castillo",
				"CEDULA": "6-714-1558",
				"PRODUCTO": "+Movil Prepago"
			},
			{
				"NO": 60,
				"NOMBRE": "Héctor Berbei",
				"CEDULA": "7-38-98",
				"PRODUCTO": "+Movil Prepago"
			},
			{
				"NO": 61,
				"NOMBRE": "Fernando Berroa",
				"CEDULA": "PE-12-57",
				"PRODUCTO": "+Movil Prepago"
			},
			{
				"NO": 62,
				"NOMBRE": "Judith Palacios",
				"CEDULA": "8-737-95",
				"PRODUCTO": "Master"
			},
			{
				"NO": 63,
				"NOMBRE": "José Smith",
				"CEDULA": "8-702-923",
				"PRODUCTO": "+Movil Prepago"
			},
			{
				"NO": 64,
				"NOMBRE": "Evelina de Boyd",
				"CEDULA": "9-55-833",
				"PRODUCTO": "+Movil Postpago"
			},
			{
				"NO": 65,
				"NOMBRE": "Frederick Espinoza",
				"CEDULA": "4-813-597",
				"PRODUCTO": "+Movil Prepago"
			},
			{
				"NO": 66,
				"NOMBRE": "Zuleika De León",
				"CEDULA": "9-728-774",
				"PRODUCTO": "+Movil Prepago"
			},
			{
				"NO": 67,
				"NOMBRE": "Alicia Afú",
				"CEDULA": "8-24-5924",
				"PRODUCTO": "Master"
			},
			{
				"NO": 68,
				"NOMBRE": "Edgardo Cartagena",
				"CEDULA": "3-111-135",
				"PRODUCTO": "+Movil Prepago"
			},
			{
				"NO": 69,
				"NOMBRE": "Carlos Miniel",
				"CEDULA": "8-817-1383",
				"PRODUCTO": "+Movil Prepago"
			},
			{
				"NO": 70,
				"NOMBRE": "Nadine Loo",
				"CEDULA": "8-819-1354",
				"PRODUCTO": "+Movil Prepago"
			},
			{
				"NO": 71,
				"NOMBRE": "Marlon Perlaza",
				"CEDULA": "8-844-233",
				"PRODUCTO": "+Movil Prepago"
			},
			{
				"NO": 72,
				"NOMBRE": "Juan Gomez",
				"CEDULA": "8-777-1154",
				"PRODUCTO": "+Movil Prepago"
			},
			{
				"NO": 73,
				"NOMBRE": "José Hun",
				"CEDULA": "8-96-483",
				"PRODUCTO": "Master"
			},
			{
				"NO": 74,
				"NOMBRE": "Roberto Fernández",
				"CEDULA": "8-953-2347",
				"PRODUCTO": "+Movil Prepago"
			},
			{
				"NO": 75,
				"NOMBRE": "Elsa Mercado",
				"CEDULA": "SB311811",
				"PRODUCTO": "+Movil Prepago"
			},
			{
				"NO": 76,
				"NOMBRE": "Juana Vega",
				"CEDULA": "7-92-1034",
				"PRODUCTO": "+Movil Prepago"
			},
			{
				"NO": 77,
				"NOMBRE": "Marlon Ríos",
				"CEDULA": "8-229-2060",
				"PRODUCTO": "+Movil Prepago"
			},
			{
				"NO": 78,
				"NOMBRE": "Yamileth Fuentes",
				"CEDULA": "8-725-256",
				"PRODUCTO": "+Movil Prepago"
			},
			{
				"NO": 79,
				"NOMBRE": "Xenia Choy",
				"CEDULA": "4-243-126",
				"PRODUCTO": "+Movil Postpago"
			},
			{
				"NO": 80,
				"NOMBRE": "Milciades Infante",
				"CEDULA": "10-7-2147",
				"PRODUCTO": "+Movil Postpago"
			},
			{
				"NO": 81,
				"NOMBRE": "José Thompson",
				"CEDULA": "3-708-296",
				"PRODUCTO": "+Movil Prepago"
			},
			{
				"NO": 82,
				"NOMBRE": "Abraham Gutierrez",
				"CEDULA": "10-705-125",
				"PRODUCTO": "Residencial"
			},
			{
				"NO": 83,
				"NOMBRE": "Carol Baxber",
				"CEDULA": "4-725-316",
				"PRODUCTO": "+Movil Prepago"
			},
			{
				"NO": 84,
				"NOMBRE": "Luciano Guerra",
				"CEDULA": "9-134-794",
				"PRODUCTO": "Residencial"
			},
			{
				"NO": 85,
				"NOMBRE": "Yulieth Meneses",
				"CEDULA": "3-737-297",
				"PRODUCTO": "+Movil Prepago"
			},
			{
				"NO": 86,
				"NOMBRE": "Abdiel Ortega",
				"CEDULA": "3-734-2132",
				"PRODUCTO": "+Movil Prepago"
			},
			{
				"NO": 87,
				"NOMBRE": "Stephanie Castro",
				"CEDULA": "8-909-418",
				"PRODUCTO": "Residencial"
			},
			{
				"NO": 88,
				"NOMBRE": "Justo Saavedra",
				"CEDULA": "6-21-297",
				"PRODUCTO": "+Movil Prepago"
			},
			{
				"NO": 89,
				"NOMBRE": "Olmedo Gil",
				"CEDULA": "8-330-735",
				"PRODUCTO": "+Movil Prepago"
			},
			{
				"NO": 90,
				"NOMBRE": "Luis Miguel Cárdenas",
				"CEDULA": "8-786-236",
				"PRODUCTO": "+Movil Prepago"
			},
			{
				"NO": 91,
				"NOMBRE": "Roberto Gordón",
				"CEDULA": "3-79-2275",
				"PRODUCTO": "+Movil Prepago"
			},
			{
				"NO": 92,
				"NOMBRE": "Evaristo Mojica",
				"CEDULA": "9-141-447",
				"PRODUCTO": "+Movil Prepago"
			},
			{
				"NO": 93,
				"NOMBRE": "Benjamín Melo",
				"CEDULA": "8-858-2087",
				"PRODUCTO": "+Movil Prepago"
			},
			{
				"NO": 94,
				"NOMBRE": "Ernesto León",
				"CEDULA": "8-161-45",
				"PRODUCTO": "Master"
			},
			{
				"NO": 95,
				"NOMBRE": "Yerlin Andreve",
				"CEDULA": "8-849-1349",
				"PRODUCTO": "+Movil Prepago"
			},
			{
				"NO": 96,
				"NOMBRE": "Nidia de Villanueva",
				"CEDULA": "8-286-22",
				"PRODUCTO": "+Movil Prepago"
			},
			{
				"NO": 97,
				"NOMBRE": "Osvaldo Quintero",
				"CEDULA": "12-702-931",
				"PRODUCTO": "+Movil Prepago"
			},
			{
				"NO": 98,
				"NOMBRE": "Andres Araúz",
				"CEDULA": "4-198-490",
				"PRODUCTO": "+Movil Prepago"
			},
			{
				"NO": 99,
				"NOMBRE": "Aralis Fernández",
				"CEDULA": "8-815-2024",
				"PRODUCTO": "+Movil Prepago"
			},
			{
				"NO": 100,
				"NOMBRE": "Angel Moreno",
				"CEDULA": "6-58-155",
				"PRODUCTO": "+Movil Prepago"
			}
		];
		var semana2 = [{
				"NO": 1,
				"NOMBRE": "ROGER SERRANO",
				"CEDULA": "4-159-774"
			},
			{
				"NO": 2,
				"NOMBRE": "LEONEL DOMINGUEZ",
				"CEDULA": "2-722-453"
			},
			{
				"NO": 3,
				"NOMBRE": "CONSUELO GONZALEZ DE MOJICA",
				"CEDULA": "8-128-46"
			},
			{
				"NO": 4,
				"NOMBRE": "ERIC GARCIA",
				"CEDULA": "8-289-110"
			},
			{
				"NO": 5,
				"NOMBRE": "ARMENIO CRUZ",
				"CEDULA": "8-371-17"
			},
			{
				"NO": 6,
				"NOMBRE": "JUAN DIAZ CAPRILES",
				"CEDULA": "8-241-200"
			},
			{
				"NO": 7,
				"NOMBRE": "FLUVIA HERNANDEZ",
				"CEDULA": "7-104-949"
			},
			{
				"NO": 8,
				"NOMBRE": "JASSAT UMER FAROOQ",
				"CEDULA": "N-19-912"
			},
			{
				"NO": 9,
				"NOMBRE": "JERRY ORIEL",
				"CEDULA": "8-702-1332"
			},
			{
				"NO": 10,
				"NOMBRE": "TOMAS ALBERTO JIMENEZ",
				"CEDULA": "8-229-2721"
			},
			{
				"NO": 11,
				"NOMBRE": "MANUEL JOSE ESPINOSA TAPIA",
				"CEDULA": "4-706-103"
			},
			{
				"NO": 12,
				"NOMBRE": "EDMUNDO GONZALEZ",
				"CEDULA": "8-414-815"
			},
			{
				"NO": 13,
				"NOMBRE": "JESUS SEALEY",
				"CEDULA": "8-855-202"
			},
			{
				"NO": 14,
				"NOMBRE": "SILKA GANTES",
				"CEDULA": "4-724-386"
			},
			{
				"NO": 15,
				"NOMBRE": "NAPOLEON RUIZ MUÃ‘OZ",
				"CEDULA": "3-725-1551"
			},
			{
				"NO": 16,
				"NOMBRE": "PATRICIA PATIÃ‘O",
				"CEDULA": "8-819-1288"
			},
			{
				"NO": 17,
				"NOMBRE": "VANESSA PLOCHE",
				"CEDULA": "8-779-750"
			},
			{
				"NO": 18,
				"NOMBRE": "MIGUEL CASTILLERO",
				"CEDULA": "8-226-397"
			},
			{
				"NO": 19,
				"NOMBRE": "EDUARDO STAGG",
				"CEDULA": "8-226-1656"
			},
			{
				"NO": 20,
				"NOMBRE": "EDILSA GUEVARA DE ALDANA",
				"CEDULA": "9-189-238"
			},
			{
				"NO": 21,
				"NOMBRE": "SANDRA BOTELLO",
				"CEDULA": "8-854-1718"
			},
			{
				"NO": 22,
				"NOMBRE": "LUIS GALAN",
				"CEDULA": "3-726-2052"
			},
			{
				"NO": 23,
				"NOMBRE": "ANA MARIA BODINGTON",
				"CEDULA": "8-154-11"
			},
			{
				"NO": 24,
				"NOMBRE": "OTILIO VILLARREAL",
				"CEDULA": "7-772-433"
			},
			{
				"NO": 25,
				"NOMBRE": "LISNARY GUERRA",
				"CEDULA": "4-749-2493"
			},
			{
				"NO": 26,
				"NOMBRE": "CARLOS EDUARDO PORTILLO",
				"CEDULA": 4322295
			},
			{
				"NO": 27,
				"NOMBRE": "MATI CORONADO RODRIGUEZ",
				"CEDULA": "8-428-640"
			},
			{
				"NO": 28,
				"NOMBRE": "ALDAIR GASMIR FORBES",
				"CEDULA": "1-741-1548"
			},
			{
				"NO": 29,
				"NOMBRE": "PEDRO RODRIGUEZ",
				"CEDULA": "8-783-1821"
			},
			{
				"NO": 30,
				"NOMBRE": "MARIA FERNANDEZ DE PEREZ",
				"CEDULA": "N-15-731"
			},
			{
				"NO": 31,
				"NOMBRE": "JUAN TERRERO PERALTA",
				"CEDULA": "5-706-1560"
			},
			{
				"NO": 32,
				"NOMBRE": "KATIUSKA PATON MUÃ‘OZ",
				"CEDULA": "8-874-1001"
			},
			{
				"NO": 33,
				"NOMBRE": "GELSY SOUSA",
				"CEDULA": "8-737-184"
			},
			{
				"NO": 34,
				"NOMBRE": "YANIRETH ARAUZ",
				"CEDULA": "3-735-2294"
			},
			{
				"NO": 35,
				"NOMBRE": "MIGUEL CORTEZ",
				"CEDULA": "7-702-2072"
			},
			{
				"NO": 36,
				"NOMBRE": "SAMUEL JOSE URRUTIA",
				"CEDULA": "8-483-13"
			},
			{
				"NO": 37,
				"NOMBRE": "ZORAYA CHAN",
				"CEDULA": "4-132-2283"
			},
			{
				"NO": 38,
				"NOMBRE": "ERICA QUINTERO",
				"CEDULA": "8-736-2321"
			},
			{
				"NO": 39,
				"NOMBRE": "JUAN BOBADILLA",
				"CEDULA": "7-703-813"
			},
			{
				"NO": 40,
				"NOMBRE": "HUMBERTO GALEGO",
				"CEDULA": "8-359-505"
			},
			{
				"NO": 41,
				"NOMBRE": "YEHAN DECEREGA",
				"CEDULA": "8-850-904"
			},
			{
				"NO": 42,
				"NOMBRE": "FRANK STROMBO",
				"CEDULA": "E-8-99679"
			},
			{
				"NO": 43,
				"NOMBRE": "JOSE CORONEL",
				"CEDULA": "1-24-952"
			},
			{
				"NO": 44,
				"NOMBRE": "LIYUN LI",
				"CEDULA": "N-21-169"
			},
			{
				"NO": 45,
				"NOMBRE": "AUGUSTO PITTY NIETO",
				"CEDULA": "4-139-84"
			},
			{
				"NO": 46,
				"NOMBRE": "MIRNA ALVEO",
				"CEDULA": "2-709-150"
			},
			{
				"NO": 47,
				"NOMBRE": "LIRIET RUTHER FORD DE RODRIGUEZ",
				"CEDULA": "3-88-393"
			},
			{
				"NO": 48,
				"NOMBRE": "ERIC BAZAN BERNAL",
				"CEDULA": "8-226-1197"
			},
			{
				"NO": 49,
				"NOMBRE": "JUDITH DIXON",
				"CEDULA": "8-327-213"
			},
			{
				"NO": 50,
				"NOMBRE": "YIDA ESCOBAR",
				"CEDULA": "9-169-795"
			},
			{
				"NO": 51,
				"NOMBRE": "BLANCA ROVETTO",
				"CEDULA": "8-394-196"
			},
			{
				"NO": 52,
				"NOMBRE": "VICTOR CORONADO",
				"CEDULA": "9-749-863"
			},
			{
				"NO": 53,
				"NOMBRE": "FANNY MARQUEZ DE GARCIA",
				"CEDULA": "6-56-1853"
			},
			{
				"NO": 54,
				"NOMBRE": "GLEIDYS MOGOLLON",
				"CEDULA": "E-8-102589"
			},
			{
				"NO": 55,
				"NOMBRE": "DIOGENES RODRIGUEZ",
				"CEDULA": "8-744-287"
			},
			{
				"NO": 56,
				"NOMBRE": "JAIME BARCELO",
				"CEDULA": "8-229-19"
			},
			{
				"NO": 57,
				"NOMBRE": "YAJAIRA BARRIA",
				"CEDULA": "8-750-540"
			},
			{
				"NO": 58,
				"NOMBRE": "AURELIO PARRA",
				"CEDULA": "8-395-557"
			},
			{
				"NO": 59,
				"NOMBRE": "MANUEL BARRIOS",
				"CEDULA": "8-449-346"
			},
			{
				"NO": 60,
				"NOMBRE": "JOSE ELETA",
				"CEDULA": "8-287-447"
			},
			{
				"NO": 61,
				"NOMBRE": "GUILLERMINA CAJAR DE COLON",
				"CEDULA": "8-93-1136"
			},
			{
				"NO": 62,
				"NOMBRE": "EDUARDO QUIROZ",
				"CEDULA": "8-309-748"
			},
			{
				"NO": 63,
				"NOMBRE": "DAYANNA ETIENNE",
				"CEDULA": "8-842-1864"
			},
			{
				"NO": 64,
				"NOMBRE": "MIRNA DEL CARMEN ARAYA AHUMADA",
				"CEDULA": "E-8-47658"
			},
			{
				"NO": 65,
				"NOMBRE": "ELISA HERNANDEZ FRANCESCHI",
				"CEDULA": "4-75-278"
			},
			{
				"NO": 66,
				"NOMBRE": "TODD CLEWETT",
				"CEDULA": "N-7561222"
			},
			{
				"NO": 67,
				"NOMBRE": "ALEJANDRA CANO",
				"CEDULA": "E-8-68835"
			},
			{
				"NO": 68,
				"NOMBRE": "GIOVANA ORTIZ GONZALEZ",
				"CEDULA": "2-706-2238"
			},
			{
				"NO": 69,
				"NOMBRE": "UBALDINA GONZALEZ",
				"CEDULA": "6-57-2149"
			},
			{
				"NO": 70,
				"NOMBRE": "RAUL MONTENEGRO",
				"CEDULA": "8-299-473"
			},
			{
				"NO": 71,
				"NOMBRE": "GILMA BRAVO DE BROCE",
				"CEDULA": "7-85-673"
			},
			{
				"NO": 72,
				"NOMBRE": "NODIER DIAZ",
				"CEDULA": "8-220-1378"
			},
			{
				"NO": 73,
				"NOMBRE": "JOSE DIAZ",
				"CEDULA": "4-730-727"
			},
			{
				"NO": 74,
				"NOMBRE": "EDUARDO BETHANCURT",
				"CEDULA": "2-706-1338"
			},
			{
				"NO": 75,
				"NOMBRE": "AMALIA DE LASSO",
				"CEDULA": "2-75-678"
			},
			{
				"NO": 76,
				"NOMBRE": "DIOSANELY CAMARENA",
				"CEDULA": "1-35-961"
			},
			{
				"NO": 77,
				"NOMBRE": "GLORIA DE RICHARDS",
				"CEDULA": "4-260-407"
			},
			{
				"NO": 78,
				"NOMBRE": "IRIS NUÃ‘EZ",
				"CEDULA": "4-241-825"
			},
			{
				"NO": 79,
				"NOMBRE": "LUZ RODRIGUEZ",
				"CEDULA": "2-81-707"
			},
			{
				"NO": 80,
				"NOMBRE": "LEONCIO JIMENEZ",
				"CEDULA": "4-194-679"
			},
			{
				"NO": 81,
				"NOMBRE": "LIDIA ALARCON",
				"CEDULA": "3-717-2147"
			},
			{
				"NO": 82,
				"NOMBRE": "ANA CRISTINA RIVERA PATIÃ‘O",
				"CEDULA": "4-784-593"
			},
			{
				"NO": 83,
				"NOMBRE": "JOSE ANGEL JORDAN BONILLA",
				"CEDULA": "3-102-767"
			},
			{
				"NO": 84,
				"NOMBRE": "ROBERTO VASQUEZ",
				"CEDULA": "8-211-252"
			},
			{
				"NO": 85,
				"NOMBRE": "CRISTIAN PEÑA",
				"CEDULA": "10-712-2109"
			},
			{
				"NO": 86,
				"NOMBRE": "ILEANA MORENO",
				"CEDULA": "8-407-423"
			},
			{
				"NO": 87,
				"NOMBRE": "LEOPOLDO DE LEON CASTILLO",
				"CEDULA": "4-705-171"
			},
			{
				"NO": 88,
				"NOMBRE": "ARIEL ALEXIS PINEDA SANCHEZ",
				"CEDULA": "4-790-219"
			},
			{
				"NO": 89,
				"NOMBRE": "RAFAEL ALBERTO ISAZA ORTEGA",
				"CEDULA": "8-230-1443"
			},
			{
				"NO": 90,
				"NOMBRE": "ISELA VILLARREAL",
				"CEDULA": "8-443-900"
			},
			{
				"NO": 91,
				"NOMBRE": "KARINA RODRIGUEZ",
				"CEDULA": "8-791-904"
			},
			{
				"NO": 92,
				"NOMBRE": "JENNIFER LOO",
				"CEDULA": "8-831-913"
			},
			{
				"NO": 93,
				"NOMBRE": "RAYDEL SIMON DE ARMA",
				"CEDULA": "950386342"
			},
			{
				"NO": 94,
				"NOMBRE": "MARTIN GARCIA MORALES",
				"CEDULA": "9-154-551"
			},
			{
				"NO": 95,
				"NOMBRE": "NORIS DEL CARMEN ARIAS LEYW",
				"CEDULA": "8-385-714"
			},
			{
				"NO": 96,
				"NOMBRE": "DANIEL GARCIA",
				"CEDULA": "4-756-877"
			},
			{
				"NO": 97,
				"NOMBRE": "MARIA DUDLEY",
				"CEDULA": "8-841-297"
			},
			{
				"NO": 98,
				"NOMBRE": "NORIEL MONTERO",
				"CEDULA": "4-773-440"
			},
			{
				"NO": 99,
				"NOMBRE": "MARCELA EDITH MADRID GUERRA",
				"CEDULA": "1-27-952"
			},
			{
				"NO": 100,
				"NOMBRE": "CEILA AYALA ROSALES",
				"CEDULA": "5-703-52"
			}
		]

		var semanas = [semana1, semana2];

		var oTable = $('#tablaganadores').DataTable({
			data: semana1,
			select: false,
			columns: [{
					data: 'NO'
				},
				{
					data: 'NOMBRE'
				},
				{
					data: 'CEDULA'
				}
				// { data: 'PRODUCTO' }
			],
			ordering: false,
			info: false,
			searching: false,
			pageLength: 25,
			responsive: true,
			pagingType: "simple_numbers",
			lengthChange: false
		});
		today = new Date()
		dayIndex = today.getDay();

		var fechas = [
			["15", "3", base_url+"public/img/premio4.png", "15 de marzo", "100"],
			["22", "3", base_url+"public/img/premio5.png", "22 de marzo", "100"],
			["28", "3", base_url+"public/img/premio1.png", "29 de marzo", "100"],
			["5", "4", base_url+"public/img/premio3.png", "5 de abril", "100"],
			["12", "4", base_url+"public/img/premio3.png", "12 de abril", "100"],
			["19", "4", base_url+"public/img/premio3.png", "19 de abril", "100"],
			["26", "4", base_url+"public/img/premio4.png", "26 de abril", "100"],
			["3", "5", base_url+"public/img/premio3.png", "3 de mayo", "100"],
			["10", "5", base_url+"public/img/premio2.png", "10 de mayo", "100"],
			["17", "5", base_url+"public/img/premio3.png", "17 de mayo", "100"],
			["24", "5", base_url+"public/img/premio3.png", "24 de mayo", "100"],
			["31", "5", base_url+"public/img/premio3.png", "31 de mayo", "100"],
			["7", "6", base_url+"public/img/premio4.png", "7 de junio", "100"],
			["14", "6", base_url+"public/img/premio1.png", "14 de junio", "100"],
			["21", "6", base_url+"public/img/premio3.png", "21 de junio", "100"],
			["28", "6", base_url+"public/img/premio3.png", "28 de junio", "100"],
			["5", "7", base_url+"public/img/premio3.png", "5 de julio", "100"],
			["12", "7", base_url+"public/img/premio3.png", "12 de julio", "100"]
		]
		var meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
		Date.prototype.getWeek = function () {
			var onejan = new Date(this.getFullYear(), 0, 1);
			return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
		}

		premio = '<div class="col-xs-5 col-sm-7 cant"><div class="col-sm-4 num"><p class="big red-f">' + fechas[0][4] + '</p></div><div class="col-sm-8  borde"><img class="center-block" src="' + fechas[0][2] + '" /></div></div><div class="col-xs-7 col-sm-5"><p class="h2">Tómbola <br> <span class="red-f">' + fechas[0][3] + '</span></p></div>';
		jQuery("#fechapasada").html(premio);

		jQuery(".semana").on("click", function () {
			jQuery(".active").removeClass("active");
			jQuery(this).addClass("active");
			//console.log(semanas[parseInt(jQuery(this).html()) - 1].length);
			if (typeof semanas[parseInt(jQuery(this).html()) - 1] == "object") {
				var semanaactual = semanas[parseInt(jQuery(this).html()) - 1];
				oTable.clear();
				jQuery.each(semanaactual, function (value, key) {
					//console.log(key);
					oTable.row.add(key);
				})
				oTable.draw();
				premio = '<div class="col-xs-5 col-sm-7 cant"><div class="col-sm-4 num"><p class="big red-f">' + fechas[parseInt(jQuery(this).html()) - 1][4] + '</p></div><div class="col-sm-8  borde"><img class="center-block" src="' + fechas[parseInt(jQuery(this).html()) - 1][2] + '" /></div></div><div class="col-xs-7 col-sm-5"><p class="h2">Tómbola <br> <span class="red-f">' + fechas[parseInt(jQuery(this).html()) - 1][3] + '</span></p></div>';
				//console.log(fechas[parseInt(jQuery(this).html()) - 1])
				jQuery("#fechapasada").html(premio);
			}
			return false;
		})

	}


});
