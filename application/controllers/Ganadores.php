<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ganadores extends CI_Controller{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

        $data = array(
            'base_url' => base_url(),
            'pagina' => 'ganadores',
            'estilo' => 'app',
            'script' => 'app',
            'class_body' => '',
            'site_name' => '100pre con la sele',
            'title' => '#100preConLaSele | Cable & Wireless Panamá',
            'description' => 'Participa por 100 premios cada semana y un Gran Premio final de B/.100,000.00 en efectivo. Promoción válida  desde el 8 de marzo hasta el 10 de julio de 2018 para clientes +Móvil prepago que recarguen B/5.00 o más, o manteniendo tu cuenta al día* para clientes postpago de +Móvil, Mast3r o CWP. *Aplican restricciones',
            'keywords' => '100pre, Siempre con la sele, Promoción, +Móvil, Mas Móvil, Mas Móvil Panamá, Prepago, Postpago, Master, +TV Digital, Cable Wireless Panamá, Selección de Panamá, CWP.',
            'ogimage' => 'share100pre',
            'tiempo_diferencia' => '',
        );

        $this->parser->parse('site/layouts/header', $data);
        $this->parser->parse('site/pages/ganadores', $data);
        $this->parser->parse('site/layouts/footer', $data);
    }

}