<div class="container-fluid main-bg">
	<div class="row">
		<div class="cable text-center">

			<div class="clock"></div>
			<div class="message"></div>
			<div class="siempre ">
				<p class="sele nop">
					<span>Siempre contigo compartiendo la pasión por nuestro equipo</span>
				</p>
				<br>
				<img src="{base_url}public/img/la-sele.png" alt="100pre con la sele" class="center-block ">
				<p class="sele">Participa por
					<span>100 premios</span>
				</p>
				<!--<p class="sele">Faltan solo <span><?php echo intval($dias); ?> días</span> para cumplir en Rusia el sueño de todos los panameños</p>-->
				<p class="sele nop"> cada semana y un Gran Premio final de B/.100,000.00 en efectivo</p>
				<!--<h2 class="sele sel">Descubre cómo puedes convertirte en uno de los <span> 1.800 ganadores +Móvil </span>, e incluso ser el <span>gran ganador de B/. 100.000.00 en efectivo </span>, solo por ser nuestro cliente.</h2>-->
				<i class="material-icons btn-menu" data-targetmenu="premios" href="#premios">keyboard_arrow_down</i>
			</div>
		</div>
		<div class="premios" id="premios">
			<div class="container">
				<div class="row">
					<div class="col-sm-offset-2 col-sm-8">
						<h2 class="text-center">Premios</h2>
						<p class="text-center">¡Uno de estos
							<span>1.801</span> premios puede ser tuyo!</p>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-offset-1  col-sm-2 col-xs-6">
						<a href="#">
							<img src="{base_url}public/img/premio3.png" class="img-responsive center-block">
						</a>
					</div>
					<div class="col-sm-2 col-xs-6">
						<a href="#">
							<img src="{base_url}public/img/premio4.png" class="img-responsive center-block">
						</a>
					</div>
					<div class="col-sm-2 col-xs-6">
						<a href="#">
							<img src="{base_url}public/img/premio5.png" class="img-responsive center-block">
						</a>
					</div>

					<div class="col-sm-2 col-xs-6">
						<a href="#">
							<img src="{base_url}public/img/premio1.png" class="img-responsive center-block">
						</a>
					</div>
					<div class="col-sm-2 col-xs-6 rescenter">
						<a href="#">
							<img src="{base_url}public/img/premio2.png" class="img-responsive center-block">
						</a>
					</div>
				</div>
				<h6 class="text-center">
					<span>Aprobado por la JCJ, Resolución 331 del 7 de marzo de 2018.</span>
				</h6>
			</div>
		</div>
		<div class="latombola">
			<div class="container">
				<div class="col-sm-offset-1 col-sm-10">
					<div class="row">
						<div class="col-md-6">
							<p class="h2">Tómbola de la semana</p>
							<p class="participa">¿Ya estás participando en
								<span> el sorteo </span> que te hará vivir mucho más fuerte tu pasión por La Sele? </p>
							<p class="slim">¡Entregaremos
								<span>1.801 </span> premios en total!</p>
						</div>
						<div class="col-md-6">
							<p class="slim text-center">Esta semana estás participando por:</p>
							<div class="row premios" id="fechafutura">
								<!--
                                <div class="col-xs-7 cant">
                                    <div class="col-sm-5 num"><p class="big">100</p></div>
                                    <div class="col-sm-7  borde"><p class="h2">iPhone 8</p><p>de 64 GB</p></div>
                                </div>
                                <div class="col-xs-5">
                                    <p class="h2">Tómbola <br> <span>15 de marzo </span></p>
                                </div>
                                -->
							</div>
						</div>
					</div>
					<div class="text-center">
						<p>¿Quieres conocer todas las tómbolas?</p>
						<a href="{base_url}tombolas" class="yellow-btn">Descúbrelas</a>
					</div>
				</div>
			</div>
		</div>
		<div class="content">
			<div class="container  mecanica" id="mecanica">
				<div class="row">
					<div class="col-sm-offset-2 col-sm-8 star">
						<h2 class="text-center">Mecánica</h2>
						<p class="text text-center">Este es el pitazo inicial desde el
							<span> 8 de marzo </span> hasta el 10 de julio de 2018. Tu que has estado
							<span>100pre con la sele </span>,  podrás participar para ganar de la siguiente forma: </p>
						<div class="row">
							<hr class="dashed">
							<div class="mec">
								<div class="col-sm-4">
									<p class="h2">
										Clientes
										<span>+Móvil</span>
										<strong>prepago</strong>
									</p>
								</div>
								<div class="col-sm-8">
									<div class="descr">
										<p>Por cada $5 de recarga el cliente prepago recibirá 1 tiquete electrónico para participar de cada tómbola electrónica.
											Participan todas las denominaciones de recargas ya sea a través de tarjetas o recargas electrónicas . Si aún no
											has recargado, haz
											<a href="http://cw.pa/recarga" target="_blank">click aquí</a>
										</p>
									</div>
								</div>
							</div>
							<div class="clear"></div>
							<hr class="dashed">
							<div class="mec colright">
								<div class="col-sm-4">
									<p class="h2">
										Clientes
										<span>+Móvil</span>
										<strong>postpago</strong>
									</p>
									<p>Individuales y empresas</p>
								</div>
								<div class="col-sm-8">
									<div class="descr">
										<p>Todo cliente móvil individual o de empresas recibirá 1 tiquete electrónico por cada $5 de la renta de su plan pospago
											mensual (no aplican consumos adicionales ni impuestos) y mantenga su cuenta al día. Para contratos individuales
											aplican clientes con planes desde $19.99 y para empresas aplican planes desde $24.99.</p>
									</div>
									<h6>*No aplican consumos adicionales ni impuesto, *El premio será entregado al representante legal de la cuenta.</h6>
								</div>
							</div>
							<div class="clear"></div>
							<hr class="dashed">
							<div class="mec">
								<div class="col-sm-4">
									<p class="h2 imgg">
										Clientes
										<img src="{base_url}public/img/master.png">
									</p>
									<p>y cobre tradicional</p>
								</div>
								<div class="col-sm-8">
									<div class="descr">
										<p>Todo cliente MASTER con 1 o más servicios (internet o tv) y mantenga su cuenta al día, recibirá 1 tiquete electrónico
											por cada $5 de renta de su plan mensual (no aplican consumos adicionales ni impuestos).
											<br> Todo cliente con servicio fijo de cobre tradicional con 2 servicios (línea, internet) y mantenga su cuenta al
											día, recibirá 1 tiquete electrónico por cada $5 de renta de su plan mensual (no aplican consumos adicionales).</p>
									</div>
									<h6>*No aplican consumos adicionales ni impuestos / ** No aplican consumos adicionales.</h6>
								</div>
							</div>
							<div class="clear"></div>
							<hr class="dashed">
							<div class="mec colright">
								<div class="col-sm-4">
									<p class="h2">
										Clientes
										<span>+TV Digital</span>
										<strong>prepago</strong>
									</p>
								</div>
								<div class="col-sm-8">
									<div class="descr">
										<p>Por cada $5 de recarga (tarjeta o recarga electrónica) y compra de plan de días el cliente recibe 1 tiquete electrónico
											para participar de cada tómbola electrónica </p>
									</div>
								</div>
							</div>
							<hr class="dashed">
						</div>
					</div>
				</div>
			</div>
			<div class="video">
				<div class="container text-center">
					<hr class="dashed">
					<p class="h2">Somos como tú, en todo momento estamos #100PreConLaSele</p>
					<br>
					<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/FRbyCcb9kLY?rel=0&amp;showinfo=0" frameborder="0"
					allow="autoplay; encrypted-media" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</div>
</div>
