<div class="container-fluid main-bg">
	<div class="cable">
		<div class="row">
			<div class="container interna">
				<h2>Términos y condiciones</h2>
			</div>
		</div>
	</div>
</div>
<div class="terminos">
	<div class="container">
		<p>Promoción válida del 8 de marzo al 10 de julio de 2018. Exclusivo para clientes de Cable & Wireless Panamá, S.A. de los
			servicios indicados a continuación:
			<br>
			<br> Clientes Prepago +Móvil recibirán un (1) boleto electrónico por cada recarga de tiempo aire realizada con tarjeta o recarga
			electrónica de B/. 5.00, en adelante.
			<br>
			<br> Clientes postpago +Móvil (Individual) con planes de B/.19.99 en adelante y que mantengan su cuenta al día recibirán un
			(1) boleto electrónico por cada B/. 5.00 de la mensualidad fija de su plan (no aplican consumos adicionales) Clientes
			postpago (Negocio) con planes de B/.24.99 en adelante y que mantengan su cuenta al día recibirán un (1) boleto electrónico
			por cada B/. 5.00 de la mensualidad fija de su plan, (no aplican consumos adicionales) Clientes de +TV Digital Prepago
			que activen un plan mensual de B/.30.00 o acumulen en activaciones de planes la suma de B/.30.00, recibirán seis (6) boletos
			electrónicos.
			<br>
			<br> Clientes Fijos Residenciales (Master) que cuenten con uno o más servicios que mantengan su cuenta al día recibirán un
			(1) boleto electrónico por cada B/. 5.00 de la mensualidad fija de su plan.
			<br>
			<br> Clientes Fijos Residenciales (Fijo Cobre) que cuenten con 2 o más servicios que mantengan su cuenta al día recibirán
			un (1) boleto electrónico por cada B/. 5.00 de la mensualidad fija de su plan.
			<br>
			<br> Serán mil ochocientos un (1,801) premios en total, repartidos así: habrá mil cien (1,100) ganadores de una (1) camiseta
			de la selección cada uno, trescientos (300) ganadores de un (1) Iphone + 20GB cada uno, doscientos (200) ganadores de
			una Tarjeta Prepagada con B/. 500.00 cada uno, cien (100) ganadores de un Televisor de 55” cada uno, cien (100) ganadores
			de una entrada al estadio con camiseta promocional y un (1) único ganador de cien mil balboas con 00/100 (B/. 100,000.00)
			en efectivo.
			<br>
			<br> Serán 18 sorteos electrónicos los cuales se realizarán en las oficinas de Cable & Wireless Panamá, Condominio Plaza Internacional,
			Vía España, las fechas de los sorteos para la selección de los ganadores de la camiseta de la Selección serán: 5, 12 y
			19 de abril, 3, 17, 24, 31 de mayo, 21 y 28 de junio, 5 y 12 de julio de 2018, en donde se escogerán cien (100) ganadores
			cada sorteo, las fechas de los sorteos para la selección de los ganadores de un (1) Iphone + 20GB serán: 15 de marzo,
			26 de abril, 07 de junio de 2018 en donde se escogerán cien (100) ganadores cada sorteo. las fechas de los sorteos para
			la selección de los ganadores de una (1) Tarjeta Prepagada con B/. 500.00 serán: 28 de marzo y 14 de junio de 2018 en
			donde se escogerán cien (100) ganadores cada sorteo. La fecha del sorteo para la selección de los cien (100) ganadores
			de un (1) Televisor de 55” será: 22 de marzo de 2018, la fecha del sorteo para la selección de los cien (100) ganadores
			de una (1) entrada al estadio con camiseta promocional será: 10 de mayo de 2018, la fecha del sorteo para la selección
			del ganador de los cien mil balboas con 00/100 (B/. 100,000.00) en efectivo será: 12 de julio de 2018.
			<br>
			<br> Los premios no son transferibles y no podrán ser cambiados por dinero. El ganador deberá ser mayor de edad. Participan
			únicamente panameños y extranjeros residentes en Panamá.
			<br>
			<br> Los ganadores serán contactados vía telefónica después de finalizada la tómbola. Los premios serán entregados en las
			oficinas de Cable & Wireless Panamá, Vía España. Los ganadores deberán presentar su cédula de identificación personal.
			En caso de resultar ganador un cliente corporativo del servicio postpago móvil se realizará una llamada al representante
			legal para que sea este quien reciba o transfiera el premio a quien considere.
			<br>
			<br> No participan los números de celulares de los colaboradores de Cable & Wireless Panamá, S.A.; Agencias Publicitarias;
			Sonitel, S.A.; Clientes de Gobierno, ni planillas asociadas. Para mayor información y consulta de listado de ganadores,
			visita nuestra página www.cwpanama.com o llame al 123. Aprobada por la Junta de Control de Juegos, Resolución N° 331 del
			7 de marzo de 2018.

		</p>
	</div>
</div>
