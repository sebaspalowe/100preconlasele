<div class="container-fluid main-bg">
	<div class="row">

		<div class="container tomb">
			<h2>Tómbolas</h2>
			<div class="col-md-offset-2 col-md-8">
				<div class="tombola">
					<div class="row">
						<div class="col-sm-3 col-xs-2">
							<img src="{base_url}public/img/copa.png">
						</div>
						<div class="col-sm-9 col-xs-10 text-left">
							<p>Todos los jueves desde el
								<span class="red-f">15 de marzo</span> hasta el
								<span class="red-f">12 de julio de 2018 </span>, en nuestra
								<strong>promo 100pre con La Sele </strong> premiaremos la fidelidad de
								<span class="red-f">1.801 fanáticos</span>, de la siguiente forma:
							</p>

						</div>
					</div>
					<div class="tablapremios">
						<table style="width:100%">
							<tr>
								<th>Semana</th>
								<th>Premio</th>
								<th>Fecha</th>
								<th>Ganadores</th>
							</tr>
							<tr>
								<td>1</td>
								<td>iPhone 8 de 64GB</td>
								<td>15 de marzo</td>
								<td>100</td>
							</tr>
							<tr>
								<td>2</td>
								<td>Televisores LED Smart TV 55”</td>
								<td>22 de marzo</td>
								<td>100</td>
							</tr>
							<tr>
								<td>3</td>
								<td>Tarjeta Prepagada con B/. 500.00</td>
								<td>29 de marzo</td>
								<td>100</td>
							</tr>
							<tr>
								<td>4</td>
								<td>Nueva camiseta oficial de La Sele</td>
								<td>5 de abril</td>
								<td>100</td>
							</tr>
							<tr>
								<td>5</td>
								<td>Nueva camiseta oficial de La Sele</td>
								<td>12 de abril</td>
								<td>100</td>
							</tr>
							<tr>
								<td>6</td>
								<td>Nueva camiseta oficial de La Sele</td>
								<td>19 de abril</td>
								<td>100</td>
							</tr>
							<tr>
								<td>7</td>
								<td>iPhone 8 de 64GB</td>
								<td>26 de abril</td>
								<td>100</td>
							</tr>
							<tr>
								<td>8</td>
								<td>Nueva camiseta oficial de La Sele</td>
								<td>3 de mayo</td>
								<td>100</td>
							</tr>
							<tr>
								<td>9</td>
								<td>Juego amistoso: Entrada + Camiseta promocional</td>
								<td>10 de mayo</td>
								<td>100</td>
							</tr>
							<tr>
								<td>10</td>
								<td>Nueva camiseta oficial de La Sele</td>
								<td>17 de mayo</td>
								<td>100</td>
							</tr>
							<tr>
								<td>11</td>
								<td>Nueva camiseta oficial de La Sele</td>
								<td>24 de mayo</td>
								<td>100</td>
							</tr>
							<tr>
								<td>12</td>
								<td>Nueva camiseta oficial de La Sele</td>
								<td>31 de mayo</td>
								<td>100</td>
							</tr>
							<tr>
								<td>13</td>
								<td>iPhone 8 de 64GB</td>
								<td>7 de junio</td>
								<td>100</td>
							</tr>
							<tr>
								<td>14</td>
								<td>Tarjeta Prepagada con B/. 500.00</td>
								<td>14 de junio</td>
								<td>100</td>
							</tr>
							<tr>
								<td>15</td>
								<td>Nueva camiseta oficial de La Sele</td>
								<td>21 de junio</td>
								<td>100</td>
							</tr>
							<tr>
								<td>16</td>
								<td>Nueva camiseta oficial de La Sele</td>
								<td>28 de junio</td>
								<td>100</td>
							</tr>
							<tr>
								<td>17</td>
								<td>Nueva camiseta oficial de La Sele</td>
								<td>5 de julio</td>
								<td>100</td>
							</tr>
							<tr>
								<td>18</td>
								<td>Nueva camiseta oficial de La Sele</td>
								<td>12 de julio</td>
								<td>100</td>
							</tr>
							<tr class="red-b">
								<td>Gran ganador</td>
								<td>Cien mil balboas
									<span>(B/. 100,000.00)</span> en efectivo</td>
								<td>12 de julio</td>
								<td>1</td>
							</tr>
						</table>
					</div>
					<h6 class="text-center">
						<span class="black-f">Aprobado por la JCJ, Resolución 331 del 7 de marzo de 2018.</span>
					</h6>
				</div>
			</div>
		</div>
	</div>
</div>
