<!DOCTYPE html>
<html lang="es">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{title}</title>
        <meta name="description" content="{description}">
        <meta name="keywords" content="{keywords}">
        <meta property="og:image" content="{ogimage}" />
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
        <link rel="icon" href="/favicon.ico" type="image/x-icon">


        <!-- Estilos -->
        <link rel="stylesheet" href="{base_url}public/css/app.css">
        <link rel="stylesheet" href="{base_url}public/css/wireless.css">
        <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css?family=Oswald:300,400,500,600,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <!-- Scripts -->
        <script>
            var base_url = "{base_url}";
            var pagina = "{pagina}";
            var diferencia_tiempo_mundial = "{tiempo_diferencia}";
        </script>


        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->
        <!-- Google Tag Manager -->
        <script>
            (function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({
                    'gtm.start': new Date().getTime(),
                    event: 'gtm.js'
                });
                var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s),
                    dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                    'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-NW6T6ZG');

        </script>
        <!-- End Google Tag Manager -->


    </head>
    <!--Contador-->

    <body id="{pagina}">
        <!--
        <nav class="btns-flotantes inicio">
    
            <a href="https://cwp.merchantprocess.net/Security/ClientLogin" target="_blank">
                <img src="https://www.cwpanama.com/images/icon_images/1517238266.png" title="Recarga aquí" alt="Recarga aquí"> 
                <p>Recarga aquí</p>
            </a>
        </nav>
        -->

        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NW6T6ZG"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->

        <div class="container-fluid">
            <div class="row">
                <div class="cable text-center">
                    <header>
                        <img src="{base_url}public/img/logo.png" alt="Logo">
                    </header>
                    <nav class="navbar navbar-default">
                        <div class="container">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div id="navbar" class="navbar-collapse collapse">
                                <ul class="nav navbar-nav ">
                                    <li>
                                        <a data-targetmenu="premios" class="btn-menu" href="<?php echo($pagina == 'home') ? '#premios' : $base_url; ?>">Premios</a>
                                    </li>
                                    <li>
                                        <a data-targetmenu="mecanica" class="btn-menu" href="<?php echo($pagina == 'home') ? '#mecanica' : $base_url; ?>">Mecánica</a>
                                    </li>
                                    <li>
                                        <a href="{base_url}tombolas">Tómbolas</a>
                                    </li>
                                    <li>
                                        <a href="{base_url}ganadores">Ganadores</a>
                                    </li>
                                    <!--<li><a data-targetmenu="contacto" class="btn-menu" href="https://promociones.cwpanama.com/MQ==/OTc=" target="_blank">Contáctenos</a></li>-->
                                    <li>
                                        <a class="btn-menu" href="{base_url}terminos-y-condiciones">Términos y Condiciones</a>
                                    </li>
                                    <li>
                                        <a href="http://cw.pa/recarga" target="_blank">Recarga en línea</a>
                                    </li>
                                </ul>
                                <ul class="nav navbar-nav navbar-right">
                                    <!--<li class="active"><a href="#">Inicio <span class="sr-only">(current)</span></a></li>-->
                                    <li>
                                        <a href="#">#100preConLaSele</a>
                                    </li>
                                    <li>
                                        <div class="redes">
                                            <a href="https://www.facebook.com/masmovilpanama/" target="_blank">
                                                <i class="fa fa-facebook"></i>
                                            </a>
                                            <a href="https://www.instagram.com/masmovilpanama/" target="_blank">
                                                <i class="fa fa-instagram"></i>
                                            </a>
                                            <a href="https://twitter.com/masmovilpanama?lang=es" target="_blank">
                                                <i class="fa fa-twitter"></i>
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <!--/.nav-collapse -->
                        </div>
                        <!--/.container-fluid -->
                    </nav>
                </div>
            </div>
        </div>
