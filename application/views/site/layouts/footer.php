<div class="container-fluid">
	<div class="row">
		<div class="content">
			<footer>
				<hr>
				<div class="container">
					<div class="align-left copy col-md-8 col-xs-12">
						<img src="{base_url}public/img/logo-2.png" alt="100pre con la sele">
						<p>Cable & Wireless Panamá, S.A. - Copyright © 2018. All Rights Reserved.</p>
					</div>
					<div class="align-left  col-md-4 col-xs-12">
						<div class="redes">
							<a href="https://www.facebook.com/masmovilpanama/" target="_blank">
								<i class="fa fa-facebook"></i>
							</a>
							<a href="https://www.instagram.com/masmovilpanama/" target="_blank">
								<i class="fa fa-instagram"></i>
							</a>
							<a href="https://twitter.com/masmovilpanama?lang=es" target="_blank">
								<i class="fa fa-twitter"></i>
							</a>
						</div>
					</div>
				</div>
			</footer>
		</div>
	</div>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="{base_url}public/js/bootstrap.min.js"></script>

<?php if($pagina == 'home') : ?>
<script src="{base_url}public/js/flipclock.js"></script>
<?php endif; ?>

<?php if($pagina == 'ganadores') : ?>
<script type="text/javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{base_url}public/js/dataTables.responsive.min.js"></script>
<?php endif; ?>

<script src="{base_url}public/js/main.js"></script>
</body>
</html>
